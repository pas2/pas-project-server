import asyncio
import websockets
import cv2
import pickle
import datetime
from scipy.spatial.distance import cosine
from tensorflow.keras.models import load_model
from sklearn.preprocessing import Normalizer
import os
from os import listdir
from keras.models import Model, Sequential
from keras.layers import Input, Convolution2D, ZeroPadding2D, MaxPooling2D, Flatten, Dense, Dropout, Activation
from PIL import Image, ImageOps
from keras.preprocessing.image import load_img, save_img, img_to_array
from keras.applications.imagenet_utils import preprocess_input
from keras.preprocessing import image
from architecture import *
import matplotlib.pyplot as plt
from keras.models import model_from_json
import tensorflow as tf
import mtcnn
import numpy as np
from tensorflow.python.keras.models import load_model
import os
import sys
from os import listdir
import base64
from io import BytesIO
import json
from pathlib import Path
import wave
import audioop
import math

os.environ["CUDA_VISIBLE_DEVICES"] = "-1"


def log(type, str):
    print(
        '[' + type + '] - ' 
        + datetime.datetime.now().strftime("%m/%d/%Y, %H:%M:%S %f") + ' - '
        + str 
    )

def logInfo(str):
    log("INFO", str)

#-------------------------
# Prepare audio data
#-------------------------

def paths_and_labels_to_dataset(audio_paths, labels):
    """Constructs a dataset of audios and labels."""
    path_ds = tf.data.Dataset.from_tensor_slices(audio_paths)
    audio_ds = path_ds.map(lambda x: path_to_audio(x))
    label_ds = tf.data.Dataset.from_tensor_slices(labels)
    return tf.data.Dataset.zip((audio_ds, label_ds))

def path_to_audio(path):
    """Reads and decodes an audio file."""
    audio = tf.io.read_file(path)
    audio, _ = tf.audio.decode_wav(audio, 1, SAMPLING_RATE)
    return audio

def audio_to_fft(audio):
    # Since tf.signal.fft applies FFT on the innermost dimension,
    # we need to squeeze the dimensions and then expand them again
    # after FFT
    audio = tf.squeeze(audio, axis=-1)
    fft = tf.signal.fft(
        tf.cast(tf.complex(real=audio, imag=tf.zeros_like(audio)), tf.complex64)
    )
    fft = tf.expand_dims(fft, axis=-1)

    # Return the absolute value of the first half of the FFT
    # which represents the positive frequencies
    return tf.math.abs(fft[:, : (audio.shape[1] // 2), :])

#-------------------------
# Noise preparation
#-------------------------

# Split noise into chunks of 16,000 steps each
def load_noise_sample(path):
    sample, sampling_rate = tf.audio.decode_wav(
        tf.io.read_file(path), desired_channels=1
    )
    if sampling_rate == SAMPLING_RATE:
        # Number of slices of 16000 each that can be generated from the noise sample
        slices = int(sample.shape[0] / SAMPLING_RATE)
        sample = tf.split(sample[: slices * SAMPLING_RATE], slices)
        return sample
    else:
        print("Sampling rate for {} is incorrect. Ignoring it".format(path))
        return None

def generate_noises():
    noise_paths = []
    for subdir in os.listdir(DATASET_NOISE_PATH):
        subdir_path = Path(DATASET_NOISE_PATH) / subdir
        if os.path.isdir(subdir_path):
            noise_paths += [
                os.path.join(subdir_path, filepath)
                for filepath in os.listdir(subdir_path)
                if filepath.endswith(".wav")
            ]
    global noises
    noises = []
    for path in noise_paths:
        sample = load_noise_sample(path)
        if sample:
            noises.extend(sample)
    noises = tf.stack(noises)

def add_noise(audio, noises=None, scale=0.5):
    if noises is not None:
        # Create a random tensor of the same size as audio ranging from
        # 0 to the number of noise stream samples that we have.
        tf_rnd = tf.random.uniform(
            (tf.shape(audio)[0],), 0, noises.shape[0], dtype=tf.int32
        )
        noise = tf.gather(noises, tf_rnd, axis=0)

        # Get the amplitude proportion between the audio and the noise
        prop = tf.math.reduce_max(audio, axis=1) / tf.math.reduce_max(noise, axis=1)
        prop = tf.repeat(tf.expand_dims(prop, axis=1), tf.shape(audio)[1], axis=1)

        # Adding the rescaled noise to audio
        audio = audio + noise * prop * scale

    return audio

#-------------------------
# Prediction
#-------------------------

def predictSpeaker(file):
    open_file = open("encodings/audio_class_names.pkl", "rb")
    class_names = pickle.load(open_file)
    open_file.close()
    print(class_names)

    valid_audio_paths = [file]
    valid_labels = [2]

    test_ds = paths_and_labels_to_dataset(valid_audio_paths, valid_labels)
    test_ds = test_ds.shuffle(buffer_size=BATCH_SIZE * 8, seed=SHUFFLE_SEED).batch(BATCH_SIZE)

    test_ds = test_ds.map(lambda x, y: (add_noise(x, noises, scale=SCALE), y))
    pred_speaker = ""
    pred_score = 0

    for audios, labels in test_ds.take(1):
        # Get the signal FFT
        ffts = audio_to_fft(audios)
        # Predict
        y_pred = model.predict(ffts) # for each audio, every prediction percentage for each
        y_pred_index = np.argmax(y_pred, axis=-1) # retrieve the index of the max prediction (ie. the class to predict)

        for index in range(1):
            # For every sample, print the true and predicted label
            # as well as run the voice with the noise
            pred_speaker = class_names[y_pred_index[index]]
            pred_score = y_pred[index][y_pred_index[index]]

            print("Predicted: {} - Value: {}".format(
                    class_names[y_pred_index[index]],
                    y_pred[index][y_pred_index[index]]
                )
            )
    # Return the predicted speaker Class name and the score
    result = '{"speaker": "' + pred_speaker + '", "score": ' + str(pred_score) + '}'
    return result

def saveWavFile(base64_sound, output_file):
    base64_wav_bytes = base64_sound.encode('utf-8')
    with open(output_file, 'wb') as file_to_save:
        decoded_wav_data = base64.decodebytes(base64_wav_bytes)
        file_to_save.write(decoded_wav_data)
    file_to_save.close()

def deleteWavFile(file):
    os.remove(file) 

#-------------------------
# Server communication and event handling
#-------------------------

async def response(websocket, path):
    while True:
        try:
            message = await websocket.recv()
            if message == "restart server":
                logInfo("Restart server!")
                os.execl(sys.executable, sys.executable, *sys.argv)
            else:
                logInfo("A request has been received from the client")
                
                logInfo("Audio file processing")
                # Encode base64 audio file to WAV
                curr_time = str(datetime.datetime.now().timestamp()).split(".")[0]
                wav_file_path = "decoded_audio" + curr_time  + ".wav"
                saveWavFile(message, wav_file_path)

                # Check if someone is speaking in the audio
                wav = wave.open(wav_file_path)
                rms = audioop.rms(wav.readframes(wav.getnframes()), wav.getsampwidth())
                decibel = 0 
                if rms > 0 :
                    decibel = 20 * math.log10(rms)
                wav.close()
                
                # Speaker prediction
                sound_threshold = 30
                prediction = ""
                # Run prediction if there is a speaker in the audio
                if decibel > sound_threshold :
                    logInfo("Start prediction")
                    prediction = predictSpeaker(wav_file_path)
                else:
                    logInfo("No prediction performed. No speaker detected.")
                    prediction = '{"speaker": "unknown", "score": 0}'

                # Remove temporary file
                deleteWavFile(wav_file_path)

                logInfo("Processing done")
                logInfo("Sending response to the client")
                await websocket.send(prediction)
        except websockets.ConnectionClosed:
            print(f"Terminated")
            break

def init():
    global model
    global peoples

    global BATCH_SIZE 
    global SHUFFLE_SEED
    global SCALE
    global SAMPLING_RATE
    global DATASET_NOISE_PATH
    global SHUFFLE_SEED
    
    BATCH_SIZE = 128
    SHUFFLE_SEED = 41
    SCALE = 0.5
    SAMPLING_RATE = 16000
    DATASET_ROOT = "audio_training"
    NOISE_SUBFOLDER = "noise"
    DATASET_NOISE_PATH = os.path.join(DATASET_ROOT, NOISE_SUBFOLDER)
    SHUFFLE_SEED = 41

    generate_noises()

    logInfo("Loading audio model")
    try:
        model = load_model('models/audio_model.h5')
        logInfo("Model loaded successfully")
    except:
        model=""
        logInfo("No model loaded")
    

if __name__ == '__main__':
    logInfo("Server started successfully")
    init()

start_server = websockets.serve(response, 'localhost', 5001)
asyncio.get_event_loop().run_until_complete(start_server)
asyncio.get_event_loop().run_forever()
