import asyncio
import websockets
import cv2
import pickle
import datetime
from scipy.spatial.distance import cosine
from tensorflow.keras.models import load_model
from sklearn.preprocessing import Normalizer
import os
from os import listdir
from keras.models import Model, Sequential
from keras.layers import Input, Convolution2D, ZeroPadding2D, MaxPooling2D, Flatten, Dense, Dropout, Activation
from PIL import Image, ImageOps
from keras.preprocessing.image import load_img, save_img, img_to_array
from keras.applications.imagenet_utils import preprocess_input
from keras.preprocessing import image
from architecture import *
import matplotlib.pyplot as plt
from keras.models import model_from_json
import tensorflow as tf
import mtcnn
import numpy as np
from tensorflow.python.keras.models import load_model
import os
import sys
from os import listdir
import base64
from io import BytesIO

os.environ["CUDA_VISIBLE_DEVICES"] = "-1"

confidence_t = 0.99
recognition_t = 0.5
required_size = (160, 160)

l2_normalizer = Normalizer('l2')


def log(str):
    print(str + datetime.datetime.now().strftime("%m/%d/%Y, %H:%M:%S %f"))


def normalize(img):
    mean, std = img.mean(), img.std()
    return (img - mean) / std


def get_face(img, box):
    x1, y1, width, height = box
    x1, y1 = abs(x1), abs(y1)
    x2, y2 = x1 + width, y1 + height
    face = img[y1:y2, x1:x2]
    return face, (x1, y1), (x2, y2)


def transformIMAGEtoCV2(pil_img):
    # use numpy to convert the pil_image into a numpy array
    open_cv_image = np.array(pil_img)
    # Convert RGB to BGR
    open_cv_image = open_cv_image[:, :, ::-1].copy()
    # resize image
    """ open_cv_image = cv2.resize(
        open_cv_image, required_size, interpolation=cv2.INTER_AREA) """
    return open_cv_image


def get_encode(model, face, size):
    face = normalize(face)
    face = cv2.resize(face, size)
    encode = model.predict(np.expand_dims(face, axis=0))[0]
    return encode


def detect(img, detector, model, encoding_dict):
    img_rgb = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    log("Before detect: ")
    results = detector.detect_faces(img_rgb)
    log("After detect: ")
    name = []

    for res in results:
        # if we are not sur that this is a face
        if res['confidence'] < confidence_t:
            continue

        face, pt_1, pt_2 = get_face(img_rgb, res['box'])
        encode = get_encode(model, face, required_size)
        encode = l2_normalizer.transform(encode.reshape(1, -1))[0]
        n = 'unknown'
        distance = float("inf")
        for db_name, db_encode in encoding_dict.items():
            dist = cosine(db_encode, encode)
            if dist < recognition_t and dist < distance:
                n = db_name
                distance = dist

        name.append(n)

    if len(name) == 0:
        name.append("No face detected")

    return name


async def response(websocket, path):
    while True:
        try:
            message = await websocket.recv()
            if message == "restart server":
                print("restart server!")
                os.execl(sys.executable, sys.executable, *sys.argv)
            else:
                log("Response begin: ")
                print(f"We got the message from the client")
                imgdata = base64.b64decode(message)
                log("Response after b64decode: ")
                im_file = BytesIO(imgdata)  # convert image to file-like object
                img = Image.open(im_file).convert(
                    'RGB')   # img is now PIL Image object
                img = ImageOps.exif_transpose(img)
                open_cv_image = transformIMAGEtoCV2(img)
                """ cv2.imshow('image', open_cv_image)

                cv2.waitKey(2000) """
                result = detect(open_cv_image, face_detector, model, peoples)
                """ filename = 'test.jpg'  # I assume you have a way of picking unique filenames
                with open(filename, 'wb') as f:
                    f.write(imgdata) """
                print("people detected: ")
                print(result)
                log("Response end: ")
                message = result
                await websocket.send('/'.join(result))
        except websockets.ConnectionClosed:
            print(f"Terminated")
            break


def load_pickle(path):
    with open(path, 'rb') as f:
        encoding_dict = pickle.load(f)
    return encoding_dict


def init():
    print("init")
    global model
    global peoples
    global face_detector

    model = load_model('models/model_no_gpu.h5')
    print("model built")
    encodings_path = 'encodings/encodings_no_gpu.pkl'
    peoples = load_pickle(encodings_path)
    print(peoples.keys())
    face_detector = mtcnn.MTCNN()

    # this is key : save the graph after loading the model
    global graph
    graph = tf.compat.v1.get_default_graph()


if __name__ == '__main__':
    init()

start_server = websockets.serve(response, 'localhost', 5000)
asyncio.get_event_loop().run_until_complete(start_server)
asyncio.get_event_loop().run_forever()
