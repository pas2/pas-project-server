module.exports = mongoose => {
    const Schema = mongoose.Schema;

    var schema = Schema(
        {
          name: String,
          images: [mongoose.ObjectId],
          audios: [mongoose.ObjectId]
        },
        { timestamps: true }
    );

    // to use id field instead of default MongoDB _id
    schema.method("toJSON", function() {
        const { __v, _id, ...object } = this.toObject();
        object.id = _id;
        return object;
    });
  
    const Person = mongoose.model("Person", schema);
    return Person;
};