const dbConfig = require("../config/db.config.js");
const MongoClient = require("mongodb");

/**
 * @param {Array} filesId: ids of files to retrieve
 * @returns {Array} Array of { url: finalFile, idFile: fileId }
 */

const getFiles = async (filesId) => {
  //Accepting user input directly is very insecure and should
  //never be allowed in a production app.
  //Sanitize the input before accepting it
  //This is for demonstration purposes only

  //Connect to the MongoDB client

  const client = await MongoClient.connect(dbConfig.url);
  if (!client) {
    return;
  }
  const db = client.db("pas");
  const collection = db.collection("fs.files");
  const collectionChunks = db.collection("fs.chunks");

  let promises = filesId.map(async (fileId) => {
    var o_id = MongoClient.ObjectID(fileId);
    return new Promise((resolve, reject) => {
      collection
        .find({ _id: o_id })
        .sort({ uploadDate: 1 })
        .toArray()
        .then(async (docs) => {
          if (!docs || docs.length === 0) {
            console.log("No files found");
            reject({
              title: "Download Error",
              message: "No file found"
            });
          } else {
            //Retrieving the chunks from the db
            await collectionChunks
              .find({ files_id: docs[0]._id })
              .sort({ n: 1 })
              .toArray()
              .then((chunks) => {
                if (!chunks || chunks.length === 0) {
                  //No data found
                  reject({
                    title: "Download Error",
                    message: "No data found"
                  });
                }
                let fileData = [];
                for (let i = 0; i < chunks.length; i++) {
                  //This is in Binary JSON or BSON format, which is stored
                  //in fileData array in base64 endocoded string format

                  fileData.push(chunks[i].data.toString("base64"));
                }

                //Display the chunks using the data URI format
                let finalFile =
                  "data:" +
                  docs[0].contentType +
                  ";base64," +
                  fileData.join("");
                resolve({
                  url: finalFile,
                  idFile: fileId,
                  contentType: docs[0].contentType
                });
              })
              .catch((err) => {
                reject({
                  title: "Download Error",
                  message: "Error retrieving chunks",
                  error: err.errmsg
                });
              });
          }
        })
        .catch((err) => {
          console.log(err);
          reject({
            title: "File error",
            message: "Error finding file",
            error: err.errMsg
          });
        });
    });
  });

  return Promise.all(promises)
    .then((res) => {
      return res;
    })
    .catch((err) => {
      console.log(err);
    });
};

module.exports = {
  getFiles: getFiles
};
