const util = require("util");
const multer = require("multer");
const GridFsStorage = require("multer-gridfs-storage");
const dbConfig = require("../config/db.config.js");

var AudioStorage = new GridFsStorage({
    url: dbConfig.url,
    options: { useNewUrlParser: true, useUnifiedTopology: true },
    file: (req, file) => {
      if (file.mimetype === 'audio/*') {
        return {
          bucketName: 'pasaudios',
          filename: `${Date.now()}-pas-${file.originalname}`
        };
      } else{
        return null;
      }
    }
});

var uploadAudios = multer({ storage: AudioStorage }).array("audios-multi-files", 10);
var uploadAudiosMiddleware = util.promisify(uploadAudios);
module.exports = uploadAudiosMiddleware;