const util = require("util");
const multer = require("multer");
const GridFsStorage = require("multer-gridfs-storage");
const dbConfig = require("../config/db.config.js");

var ImageStorage = new GridFsStorage({
  url: dbConfig.url,
  options: { useNewUrlParser: true, useUnifiedTopology: true },
  file: (req, file) => {
    console.log("file");
    console.log(file);
    if (file.mimetype === "image/*") {
      return {
        bucketName: "pasimages",
        filename: `${Date.now()}-pas-${file.originalname}`
      };
    } else {
      return null;
    }
  }
});

var uploadImages = multer({ storage: ImageStorage }).array(
  "images-multi-files",
  10
);
var uploadImagesMiddleware = util.promisify(uploadImages);
module.exports = uploadImagesMiddleware;
