module.exports = app => {
   const uploadController = require("../controllers/upload.controller.js");
   const router = require("express").Router();

   // Upload new image
   router.post("/uploadImages", uploadController.uploadImageFiles);

   // Upload new audio clip
   router.post("/uploadAudios", uploadController.uploadAudioFiles);

   app.use('/filesapi', router);
};