module.exports = app => {
    const getFilesController = require("../controllers/getFiles.controller.js");
    const router = require("express").Router();
 
    // Retrieve files (images / audio clips)
    router.post("/getFiles", getFilesController.getFiles);
 
 
    app.use('/filesapi', router);
 };