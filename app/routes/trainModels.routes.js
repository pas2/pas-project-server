module.exports = (app) => {
  const trainModelsController = require("../controllers/trainModels.controller.js");
  const router = require("express").Router();

  // Train models (images and audios)
  router.post("/trainModels", trainModelsController.trainModels);

  app.use("/trainapi", router);
};
