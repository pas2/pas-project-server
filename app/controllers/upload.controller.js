const uploadImages = require("../middleware/uploadImages");
const uploadAudios = require("../middleware/uploadAudios");

const uploadImageFiles = async (req, res) => {
  try {
    await uploadImages(req, res);
    if (req.files.length <= 0) {
      return res.send(`You must select at least 1 file.`);
    }
    console.log("into upload controller");
    console.log(req.files);
    return res.json(req.files);
  } catch (error) {
    console.log(error);

    if (error.code === "LIMIT_UNEXPECTED_FILE") {
      return res.send("Too many files to upload.");
    }
    return res.send(`Error when trying upload many files: ${error}`);
  }
};

const uploadAudioFiles = async (req, res) => {
  try {
    await uploadAudios(req, res);

    if (req.files.length <= 0) {
      return res.send(`You must select at least 1 file.`);
    }

    return res.json(req.files);
  } catch (error) {
    console.log(error);

    if (error.code === "LIMIT_UNEXPECTED_FILE") {
      return res.send("Too many files to upload.");
    }
    return res.send(`Error when trying upload many files: ${error}`);
  }
};

module.exports = {
  uploadImageFiles: uploadImageFiles,
  uploadAudioFiles: uploadAudioFiles
};
