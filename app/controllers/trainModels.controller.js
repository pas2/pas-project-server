const fs = require("fs");
const util = require("util");
const exec = util.promisify(require("child_process").exec);
const path = require("path");
const getFile = require("../middleware/getFiles");
require("dotenv").config();

const removeDir = function (pathToDir) {
  if (fs.existsSync(pathToDir)) {
    const files = fs.readdirSync(pathToDir);

    if (files.length > 0) {
      files.forEach(function (filename) {
        if (fs.statSync(pathToDir + "/" + filename).isDirectory()) {
          removeDir(pathToDir + "/" + filename);
          fs.rmdirSync(pathToDir + "/" + filename);
        } else {
          fs.unlinkSync(pathToDir + "/" + filename);
        }
      });
    } else {
      console.log("No files found in the directory.");
    }
  } else {
    console.log("Directory path not found.");
  }
};

const trainModels = async (req, res) => {
  console.log("Into trainModels controller");
  console.log(req.body);

  let persons = req.body.persons || [];
  let trainImage = req.body.options.image || false;
  let trainAudio = req.body.options.audio || false;
  if (persons.length === 0) {
    return res.status(300).send("No persons to train");
  }

  //// delete all images files on database
  const directory = "../../database";
  const pathToDir = path.join(__dirname, directory);
  console.log(pathToDir);
  removeDir(pathToDir);

  //// recreate all database with mongo files stored
  // Create Database folder
  var dir = "./database";
  if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir);
  }

  let promises = persons.map((person) => {
    return new Promise(async (resolve, reject) => {
      console.log(person.name);
      // create images
      if (person.images.length > 0) {
        // create images folder if needed
        var dir = "./database/images/" + person.name;
        if (!fs.existsSync(dir)) {
          fs.mkdirSync(dir, { recursive: true });
        }
        // retrieve array of files in base64 and files ids
        let result = await getFile.getFiles(person.images);
        // store images retrieved
        result.forEach((file) => {
          var base64Data = file.url.replace(
            /^data:image\/(png|jpg|jpeg);base64,/,
            ""
          );
          const buffer = Buffer.from(base64Data, "base64");
          fs.writeFileSync(
            dir + "/" + file.idFile + "." + file.contentType.split("/")[1],
            buffer
          );
        });
        resolve(result);
      }

      // create audio files
      if (person.audios.length > 0) {
        // create images folder if needed
        var dir = "./database/audios/" + person.name;
        if (!fs.existsSync(dir)) {
          fs.mkdirSync(dir, { recursive: true });
        }
        // retrieve array of files in base64 and files ids
        let result = await getFile.getFiles(person.audios);
        // store images retrieved
        result.forEach((file) => {
          var base64Data = file.url.split(",")[1];
          const buffer = Buffer.from(base64Data, "base64");
          // let extension = "mp3";
          // if (file.contentType.split("/")[1] == "x-m4a") {
          //   extension = "m4a";
          // }
          let contentType_extension = file.contentType.split("/")[1];
          console.log("CONTENT_TYPE_EXTENSION = ", contentType_extension);
          let extension = "wav";
          if (contentType_extension == "x-m4a") extension = "m4a";
          if (contentType_extension == "mpeg") extension = "mp3";

          fs.writeFileSync(dir + "/" + file.idFile + "." + extension, buffer);
        });
        resolve(result);
      }
    });
  });
  let result = await Promise.all(promises)
    .then((data) => {
      return data;
    })
    .catch((err) => {
      console.log(err);
      res.status(300).send(err);
    });

  //// Start training of models
  // Train Facial recognition model if necessary
  if (trainImage) {
    let environmentName = "websocketServer";
    let pythonScript = "train_v2_no_gpu.py";
    // to find your python location in anaconda env
    // Activate your conda environement then do the following cmd:
    // "where python"
    // take the first line

    let pythonDir = process.env.pythondPath + environmentName + "\\python.exe";
    let currentWorkingDirectory =
      process.env.wdPath + "\\websocket_server\\training\\";

    const { error, stdout, stderr } = await exec(
      pythonDir + " " + pythonScript,
      {
        cwd: currentWorkingDirectory
      }
    );
    if (error) {
      console.error(`error: ${error}`);
      res.status(300).send("Train facial recognition model error");
    }
    console.log(`stdout: ${stdout}`);
  }

  // Train speaker recognition model if necessary
  if (trainAudio) {
    console.log("TRAIN AUDIO");
    let environmentName = "websocketServer";
    let audio_python_script = "speaker_recognition_model.py";
    let pythonDir = process.env.pythondPath + environmentName + "\\python.exe";
    let currentWorkingDirectory =
      process.env.wdPath + "\\websocket_server\\training\\";
    let audio_python_script_path =
      currentWorkingDirectory + audio_python_script;
    console.log(audio_python_script_path);
    let spawn = require("child_process").spawn;
    let audio_python_script_command = spawn(
      pythonDir,
      [audio_python_script_path],
      { cwd: currentWorkingDirectory }
    );

    audio_python_script_command.stdout.on("data", function (data) {
      console.log("[AUDIO_SCRIPT] stdout: " + data.toString());
    });

    audio_python_script_command.stderr.on("data", function (data) {
      console.log("[AUDIO_SCRIPT] stderr: " + data.toString());
    });

    audio_python_script_command.on("exit", function (code) {
      console.log(
        "[AUDIO_SCRIPT] child process exited with code " + code.toString()
      );
    });
  }

  res.status(200).send("Train face recognition model successfully");
};

module.exports = {
  trainModels: trainModels
};
