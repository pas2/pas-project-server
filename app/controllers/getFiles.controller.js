const getFile = require("../middleware/getFiles");

const getFiles = async (req, res) => {
  try {
    console.log("Into get file controller");
    console.log(req.body);
    if (req.body.filesId.length <= 0) {
      return res.send(`You must select at least 1 file.`);
    }
    let result = await getFile.getFiles(req.body.filesId);
    return res.status(200).send(result);
  } catch (error) {
    console.log(error);

    if (error.code === "LIMIT_UNEXPECTED_FILE") {
      return res.status(300).send("Too many files to upload.");
    }
    return res
      .status(300)
      .send(`Error when trying upload many files: ${error}`);
  }
};

module.exports = {
  getFiles: getFiles
};
