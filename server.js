const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const fs = require("fs");

const app = express();

var corsOptions = {
  origin: "http://localhost:8080"
};

app.use(cors(corsOptions));

// parse requests of content-type - application/json
app.use(bodyParser.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

const db = require("./app/models");
db.mongoose
  .connect(db.url, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  })
  .then(() => {
    console.log("Connected to the database!");
  })
  .catch((err) => {
    console.log("Cannot connect to the database!", err);
    process.exit();
  });

app.get("/", (req, res) => {
  res.send("Hello World!");
});

app.get("/download/model", function (req, res) {
  const file = "./facial_classifier/model/model.json";
  res.download(file); // Set disposition and send it.
});

// usefull to download the different shard of the model
app.get("/download/:shard(*bin$)", function (req, res) {
  const path = `./facial_classifier/model/${req.params.shard}`;
  try {
    if (fs.existsSync(path)) {
      console.log(path + " exist");
      res.download(path);
    } else {
      console.log(path + " not exist");
      res.statusCode = 500;
      res.send("error: " + req.params.shard + " not exist");
      res.end();
    }
  } catch (err) {
    console.error(err);
    res.statusCode = 500;
    res.send("error");
    res.end();
  } // Set disposition and send it.
});

require("./app/routes/person.routes")(app);
require("./app/routes/upload.routes")(app);
require("./app/routes/getFile.routes")(app);
require("./app/routes/trainModels.routes")(app);

const port = process.env.PORT || 4500;

app.listen(port, () => {
  console.log(`Example app listening on port ${port}!`);
});
